/*C program which writes a sentence to a file, reads and appending another line to file*/

#include <stdio.h>
int main(){
    FILE *fp;
    char str[100];
    //Create a file named "assignment9.txt" and open it in write mode.
    fp = fopen("assignment9.txt","w");
    fprintf(fp,"%s","UCSC is one of the leading institutes in Sri Lanka for Computing Studies.");
    fclose(fp);

    /*Open the file in read mode and read the text from the file
     and print it to the output screen */
    fp = fopen("assignment9.txt","r");
        fgets(str,100,fp);
        printf("%s\n",str);
        fclose(fp);

    //Open the file in append mode and append a sentence
    fp = fopen("assignment9.txt","a");
        fprintf(fp,"%s","\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing field.");

    fclose(fp);
    return 0;
}

